package ao.test.base.extension;

import ao.test.util.JsonUtil;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.matchers.text.ValuePrinter;
import org.mockito.invocation.InvocationOnMock;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.withSettings;

public class JsonPrinterExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    private static final String VALUE_PRINTER_MOCKED_STATIC = "valuePrinterMockedStatic";

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        getStore(context).put(VALUE_PRINTER_MOCKED_STATIC,
                Mockito.mockStatic(ValuePrinter.class, withSettings().defaultAnswer(CALLS_REAL_METHODS)));
        Mockito.when(ValuePrinter.print(any())).thenAnswer(this::jsonPrint);
    }

    private String jsonPrint(InvocationOnMock invocationOnMock) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Object[] arguments = invocationOnMock.getArguments();
        if (arguments.length == 0) {
            return null;
        }
        if (arguments.length != 1) {
            return JsonUtil.toJson(arguments);
        }
        Object argument = arguments[0];
        if (argument.getClass().getName().equals("org.mockito.internal.matchers.text.FormattedText")) {
            Method getTextMethod = argument.getClass().getDeclaredMethod("getText");
            getTextMethod.setAccessible(true);
            return JsonUtil.toJson(getTextMethod.invoke(argument));
        }
        return JsonUtil.toJson(argument);
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        getStore(context).get(VALUE_PRINTER_MOCKED_STATIC, MockedStatic.class).close();
    }

    private Store getStore(ExtensionContext context) {
        return context.getStore(Namespace.create(getClass(), context.getRequiredTestMethod()));
    }

}
