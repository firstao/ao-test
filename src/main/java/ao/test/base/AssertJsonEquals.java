package ao.test.base;


import ao.test.util.JsonUtil;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * json比较断言类
 * 将对象转换成json后再比较
 * @author aohong
 */
public class AssertJsonEquals {

    private AssertJsonEquals() {
    }

    public static void assertJsonEquals(Object expected, Object actual) {
        assertJsonEquals(expected, actual, null);
    }

    public static void assertJsonEquals(Object expected, Object actual, String message) {
        assertJsonEquals(expected, actual, message, (String[]) null);
    }

    public static void assertJsonEquals(Object expected, Object actual, String message, String... excludeFields) {
        final boolean expectedIsString = expected instanceof String;
        final boolean actualIsString = actual instanceof String;
        if (excludeFields == null && !expectedIsString && !actualIsString) {
            Assertions.assertEquals(toJson(expected), toJson(actual), message);
        } else {
            final String expectedString;
            if (!expectedIsString) {
                expectedString = toJson(toJson(expected), excludeFields);
            } else {
                expectedString = toJson(expected, excludeFields);
            }
            final String actualString;
            if (!actualIsString) {
                actualString = toJson(toJson(actual), excludeFields);
            } else {
                actualString = toJson(actual, excludeFields);
            }
            Assertions.assertEquals(expectedString, actualString, message);
        }
    }

    private static String toJson(Object object, String... excludeFields) {
        Object o = object;
        if (object instanceof String) {
            final String s = (String) object;
            if (s.trim().startsWith("[")) {
                final List list = JsonUtil.fromJson(s, List.class, HashMap.class);
                for (Object map : list) {
                    exclude((Map) map, excludeFields);
                }
                o = list;
            } else {
                final HashMap map = JsonUtil.fromJson(s, HashMap.class);
                exclude(map, excludeFields);
                o = map;
            }
        }
        return JsonUtil.toJson(o, true);
    }

    private static void exclude(Map map, String[] excludeFields) {
        if (excludeFields != null) {
            for (String excludeField : excludeFields) {
                map.put(excludeField, null);
            }
        }
    }

    public static void assertJsonEqualsExcludeFields(Object expected, Object actual, String... excludeFields) {
        assertJsonEquals(expected, actual, null, excludeFields);
    }

    public static void assertJsonEqualsExcludeIds(Object expected, Object actual, String... excludeFields) {
        final List<String> ids = Arrays.asList("id", "createAt", "updateAt");
        if (excludeFields == null) {
            String[] efs = new String[ids.size()];
            efs = ids.toArray(efs);
            assertJsonEqualsExcludeFields(expected, actual, efs);
        } else {
            final List<String> list = Arrays.stream(excludeFields).collect(Collectors.toList());
            list.addAll(ids);
            String[] efs = new String[list.size()];
            efs = ids.toArray(efs);
            assertJsonEqualsExcludeFields(expected, actual, efs);
        }
    }

}
