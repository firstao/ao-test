package ao.test.java8;

import org.junit.jupiter.api.Test;

public class AnnotationTest {

    @Test
    public void testFunction() {
        Hint hint = Person.class.getAnnotation(Hint.class);
        System.out.println(hint);                   // null

        Hints hints1 = Person.class.getAnnotation(Hints.class);
        System.out.println(hints1);                   // null
//        System.out.println(hints1.value().length);  // 2

        Hint[] hints2 = Person1.class.getAnnotationsByType(Hint.class);
        System.out.println(hints2);                   // null
        System.out.println(hints2.length);          // 2
    }
}
