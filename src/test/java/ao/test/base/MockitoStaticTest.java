package ao.test.base;

import ao.test.util.JsonUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.exceptions.base.MockitoException;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.mockito.ArgumentMatchers.any;

/**
 * @author aohong
 */
@MockitoSettings(strictness = Strictness.WARN)
public class MockitoStaticTest {

    @Test
    void mockStaticWithScope() {
        // Mock scope
        try (MockedStatic<JsonUtil> mocked = Mockito.mockStatic(JsonUtil.class)) {
            Mockito.when(JsonUtil.toJson(any())).thenReturn("{any}");
            Assertions.assertEquals("{any}", JsonUtil.toJson("any"));

            mocked.when(() -> JsonUtil.toJson(null)).thenReturn("{}");
            Assertions.assertEquals("{}", JsonUtil.toJson(null));
            mocked.when(() -> JsonUtil.toJson(null)).thenCallRealMethod();
//            Assertions.assertEquals("", JsonUtil.toJson(null));

            Mockito.when(JsonUtil.toJson(any())).thenReturn("{any}");
//            Assertions.assertEquals("{any}", JsonUtil.toJson(new ResultDTO()));
            Assertions.assertEquals("{any}", JsonUtil.toJson(null));
            System.out.println(JsonUtil.getUseAnnotationMapper());
            mocked.when(() -> JsonUtil.getUseAnnotationMapper()).thenCallRealMethod();
            System.out.println(JsonUtil.getUseAnnotationMapper());
        }
        String result = JsonUtil.toJson(null);
        Assertions.assertNotEquals("{}", result);
        Assertions.assertEquals("", result);
        System.out.println(JsonUtil.getUseAnnotationMapper());
    }

    @Test
    void mockStatic() {
        MockedStatic<JsonUtil> mocked = Mockito.mockStatic(JsonUtil.class);
        mocked.when(() -> JsonUtil.toJson(null)).thenReturn("{}");
        mocked.when(() -> JsonUtil.toJson(new Object())).thenReturn("{Object}");
        Object o = new Object();
        mocked.when(() -> JsonUtil.toJson(o)).thenReturn("{Object}");

        Assertions.assertEquals("{}", JsonUtil.toJson(null));
        // 重写了equals和hashCode方法的对象
        Assertions.assertNotEquals("{Object}", JsonUtil.toJson(new Object()));
        Assertions.assertEquals("{Object}", JsonUtil.toJson(o));

        mocked.close();
        MockedStatic<JsonUtil> mocked1 = Mockito.mockStatic(JsonUtil.class);
//        Mockito.mockStatic(JsonUtil.class);
//        org.mockito.exceptions.base.MockitoException:
//        For qsq.biz.common.util.JsonUtil, static mocking is already registered in the current thread
//        To create a new mock, the existing static mock registration must be deregistered

        // 每个线程只能够mockStatic一个Util，第二次mock会报错，所以要用上述mockStaticWithScope的方式
        Assertions.assertThrows(MockitoException.class,
                () -> Mockito.mockStatic(JsonUtil.class));
        mocked1.close();
    }

}
