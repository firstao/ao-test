package ao.test.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

class JsonUtilTest {

    @Test
    void fromJson() {
        System.out.println(JsonUtil.fromJson(null, List.class, Map.class, String.class, List.class, Integer.class));
        System.out.println(JsonUtil.fromJson("[]", List.class, Map.class, String.class, List.class, Integer.class));
        System.out.println(JsonUtil.fromJson("[{}]", List.class, Map.class, String.class, List.class, Integer.class));
        List<Map<String, List<Integer>>> list = JsonUtil.fromJson("[{\"a\":[1,2]}]", List.class, Map.class, String.class, List.class, Integer.class);
        System.out.println(list);
        Assertions.assertEquals(2, list.get(0).get("a").get(1));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class TestData {
        Integer a;
    }

    @Test
    void testClone() {
        TestData testData = new TestData(1);
        TestData clone = JsonUtil.clone(testData);
        Assertions.assertEquals(testData, clone);
        Assertions.assertNotSame(testData, clone);
    }

    @Test
    void t() {
        System.out.println(JsonUtil.toJson(new TestData(2)));
        Assertions.assertEquals("{\"a\":2}", JsonUtil.toJson(new TestData(2)));
    }
}