package ao.test.util;

import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Set;

/**
 * JsonFileReadUtil Tester.
 *
 * @author aohong
 * @version 1.0
 * @since <pre>Aug 27, 2019</pre>
 */
public class JsonFileReadUtilTest {

    @BeforeEach
    public void before() throws Exception {
        System.out.println("--------before------");
    }

    @AfterEach
    public void after() throws Exception {
        System.out.println("--------after------");
    }


    @Test
    public void readFile() {
        String s = JsonFileReadUtil.readFile("/data/1.json");
        System.out.println(s);
    }

    @Test
    public void readDir() {
        Map<String, String> testDataList =
                JsonFileReadUtil.readStringFromDir("/data1/");
        System.out.println(testDataList);
    }

    public static Set<Map.Entry<String, String>> list() throws Exception {
        return JsonFileReadUtil
                .readStringFromDir("/data1/")
                .entrySet();
    }

    @Data
    static class TestData {
        Integer test;
    }


}
