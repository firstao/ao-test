package ao.test.base;

import ao.test.util.JsonUtil;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * JsonFileDirTestBase
 *
 * @author aohong
 */
public class CsvFileDirTest {

    @Data
    static class TestData {
        int test;
        Double doubleValue;
        BigDecimal bigDecimal;
        Date date;
        String string;
        TestData inner;
        List<Integer> list;
        int result;
    }

    static class TestData1 extends TestData {

        public static TestData1 of(String string) {
            return JsonUtil.fromJson(string, TestData1.class);
        }
    }

    @BeforeEach
    void beforeEach(TestInfo testInfo) {
        System.out.println("--------before------");
        System.out.println(testInfo);
    }

    @AfterEach
    public void after() throws Exception {
        System.out.println("--------after------");
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/csv/1.csv", numLinesToSkip=1)
    public void testConvertWith(@ConvertWith(JsonStringToObjectConverter.class) TestData testData, Integer i,
            @ConvertWith(JsonStringToObjectConverter.class) TestData testData1) {
        System.out.println(testData);
        System.out.println(i);
        System.out.println(JsonUtil.toJson(testData));
        System.out.println(JsonUtil.toJson(testData1));
        Assertions.assertEquals(testData.result, testData.test * 10);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/csv/1.csv", numLinesToSkip=1)
    public void testOf(TestData1 testData, Integer i, TestData1 testData1) {
        System.out.println(testData);
        System.out.println(i);
        System.out.println(JsonUtil.toJson(testData));
        System.out.println(JsonUtil.toJson(testData1));
        Assertions.assertEquals(testData.result, testData.test * 10);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/csv/1.csv", numLinesToSkip=1)
    public void testArgumentsAccessor(ArgumentsAccessor arguments) {
        // 也依赖TestData里的静态工厂方法
        TestData1 testData = arguments.get(0, TestData1.class);
        Integer i = arguments.getInteger(1);
        TestData1 testData1 = arguments.get(2, TestData1.class);
        System.out.println(testData);
        System.out.println(i);
        System.out.println(JsonUtil.toJson(testData));
        System.out.println(JsonUtil.toJson(testData1));
        Assertions.assertEquals(testData.result, testData.test * 10);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/csv/2.csv")
    public void testConvertWith2(String s, Double d, Integer i,
            @ConvertWith(JsonStringToObjectConverter.class) TestData testData) {
        System.out.println(i);
        System.out.println(JsonUtil.toJson(testData));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/csv/2.csv")
    public void testJsonStringToObject(String s, Double d, Integer i, @JsonStringToObject TestData testData) {
        System.out.println(i);
        System.out.println(JsonUtil.toJson(testData));
    }

}
