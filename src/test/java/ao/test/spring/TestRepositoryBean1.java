package ao.test.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 * @author aohong
 */
@Repository
@Slf4j
@Qualifier("testRepositoryBean")
@Offline
@Order(3)
public class TestRepositoryBean1 implements TestAble {

    public void test() {
        log.info("TestRepositoryBean test1");
    }
}
