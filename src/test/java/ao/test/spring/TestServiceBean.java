package ao.test.spring;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

/**
 * @author aohong
 */
@Service
@AllArgsConstructor
@Slf4j
public class TestServiceBean {

    @Nullable
    private TestRepositoryBean testRepositoryBean;

    public void test() {
        log.info("TestServiceBean test");
        if (testRepositoryBean != null) {
            testRepositoryBean.test();
        }
    }

}
