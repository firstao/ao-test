package ao.test.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * @author aohong
 */
@Repository
@Slf4j
@Qualifier("testRepositoryBean")
public class TestRepositoryBean implements TestAble {

    public void test() {
        log.info("TestRepositoryBean test");
    }
}
