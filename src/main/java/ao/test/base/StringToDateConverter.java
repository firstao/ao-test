package ao.test.base;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

/**
 * String转成Date对象，转换器
 */
public class StringToDateConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        if (source instanceof String) {
//            return DateUtil.parseDate((String)source);
        }
        return source;
    }
}
