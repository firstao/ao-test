package ao.test.base;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author aohong
 */
public class MockNowTest {

    @Test
    @MockNow
    void testMockLocalDateTime() {
        Assertions.assertEquals("2021-01-01 18:11:12+0800", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
        Assertions.assertEquals("2021-01-01 18:11:12+0800", DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
    }

    @Test
    @MockNow({"2021-01-02T10:11:12+0000", "2021-01-02T10:11:12+0800"})
    void testMockDate() {
        Date date = new Date();
        Date date1 = new Date();
        Assertions.assertEquals("2021-01-02 18:11:12+0800", DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
        Assertions.assertEquals("2021-01-02 10:11:12+0800", DateFormatUtils.format(date1, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
        Assertions.assertEquals("2021-01-02 10:11:12+0000", DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
        Assertions.assertEquals("2021-01-02 02:11:12+0000", DateFormatUtils.format(date1, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
    }

    @Test
    @MockNow({"2021-01-02T10:11:12+0000"})
    void testMockCalendar() {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("GMT-0:00"));
        Assertions.assertNotSame(calendar, calendar1);
        Assertions.assertEquals("2021-01-02 18:11:12+0800", DateFormatUtils.format(calendar, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
        Assertions.assertEquals("2021-01-02 18:11:12+0800", DateFormatUtils.format(calendar1, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+8")));
        Assertions.assertEquals("2021-01-02 10:11:12+0000", DateFormatUtils.format(calendar, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
        Assertions.assertEquals("2021-01-02 10:11:12+0000", DateFormatUtils.format(calendar1, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
        System.out.println(DateFormatUtils.format(calendar, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
        System.out.println(calendar.getTime());
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        System.out.println(DateFormatUtils.format(calendar, "yyyy-MM-dd HH:mm:ssZ", TimeZone.getTimeZone("GMT+0")));
        System.out.println(calendar.getTime());
    }


    @Test
    @MockNow("2021-01-02 10:11:12")
    void testMockLocalDateTime2() {
        Assertions.assertEquals("2021-01-02T10:11:12", LocalDateTime.now().toString());
        Assertions.assertEquals("2021-01-02T10:11:12", LocalDateTime.now().toString());
    }

    @Test
    @MockNow({"2021-04-01 10:11:12", "2021-04-02 10:11:12", "2021-04-03 10:11:12"})
    void testMockLocalDateTime1() {
        final LocalDateTime localDateTime4 = LocalDateTime.parse("2021-04-01 10:11:12",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(localDateTime4);
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTime1 = LocalDateTime.now();
        LocalDateTime localDateTime2 = LocalDateTime.now();
        LocalDateTime localDateTime3 = LocalDateTime.now();
        Assertions.assertEquals("2021-04-01T10:11:12", localDateTime.toString());
        Assertions.assertEquals("2021-04-02T10:11:12", localDateTime1.toString());
        Assertions.assertEquals("2021-04-03T10:11:12", localDateTime2.toString());
        Assertions.assertEquals("2021-04-03T10:11:12", localDateTime3.toString());
    }

}
