package ao.test.base.matchers;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
class TestData {
    int a;

    public boolean test(TestData testData) {
        return true;
    }
}
