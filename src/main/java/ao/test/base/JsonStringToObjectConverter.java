package ao.test.base;

import ao.test.util.JsonUtil;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

/**
 * JsonString转成Java对象，转换器
 * @author aohong
 */
public class JsonStringToObjectConverter extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        if (source instanceof String) {
            return JsonUtil.fromJson((String)source, targetType);
        }
        return source;
    }
}
