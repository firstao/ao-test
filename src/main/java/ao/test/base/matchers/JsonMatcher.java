package ao.test.base.matchers;

import ao.test.base.AssertJsonEquals;

import static org.mockito.ArgumentMatchers.argThat;

/**
 * 验证是否为json值相等
 * @author aohong
 */
public class JsonMatcher {

    public static <T> T eq(T wanted) {
        return argThat(argument -> {
            AssertJsonEquals.assertJsonEquals(wanted, argument);
            return true;
        });
    }

}
