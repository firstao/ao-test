package ao.test.base.extension;

import ao.test.base.MockNow;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.mockito.Mockito.*;

/**
 * @author aohong
 */
public class MockNowExtension implements BeforeTestExecutionCallback, AfterTestExecutionCallback {
    private static final String DATE_MOCKED_CONSTRUCTION = "dateMockedConstruction";
    private static final String CALENDAR_MOCK = "calendarMock";
    private static final String LOCAL_DATE_TIME_MOCK = "localDateTimeMock";
    private static final String LOCAL_DATE_MOCK = "localDateMock";
    private static final String LOCAL_TIME_MOCK = "localTimeMock";
    private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Override
    public void beforeTestExecution(ExtensionContext extensionContext) throws Exception {
        String[] values = extensionContext.getElement().get().getAnnotation(MockNow.class).value();
        Validate.notEmpty(values);

        Date[] mockDates = Arrays.stream(values).map(this::parseDate).toArray(Date[]::new);
        MockedConstruction<Date> dateMockedConstruction = mockConstruction(Date.class,
                withSettings().useConstructor().defaultAnswer(CALLS_REAL_METHODS),
                (mock, context) -> {
                    try {
                        if (context.arguments().size() == 0) {
                            // 只 mock 掉 new Date()
                            if (context.getCount() < mockDates.length) {
                                mock.setTime(mockDates[context.getCount() - 1].getTime());
                            } else {
                                mock.setTime(mockDates[mockDates.length - 1].getTime());
                            }
                        } else {
                            // new Date(long timestamp)，设置实际值
                            mock.setTime((Long) context.arguments().get(0));
                        }
                    } catch (Exception e) {
                        System.out.println("忽略即可，MockNow时不影响使用的异常start");
                        e.printStackTrace();
                        System.out.println("忽略即可，MockNow时不影响使用的异常end");
                    }
                });
        final Store store = getStore(extensionContext);
        store.put(DATE_MOCKED_CONSTRUCTION, dateMockedConstruction);

        MockedStatic<Calendar> calendarMockedStatic = mockStatic(Calendar.class, withSettings().defaultAnswer(CALLS_REAL_METHODS));
        store.put(CALENDAR_MOCK, calendarMockedStatic);
        thenAnswer(mockDates, calendarMockedStatic.when(Calendar::getInstance));
        thenAnswer(mockDates, calendarMockedStatic.when(() -> Calendar.getInstance(TimeZone.getTimeZone("GMT-0:00"))));

        LocalDateTime[] localDateTimes = Arrays.stream(values).map(this::parseLocalDateTime).filter(Objects::nonNull)
                .toArray(LocalDateTime[]::new);
        if (localDateTimes.length > 0) {
            store.put(LOCAL_DATE_TIME_MOCK,
                    Mockito.mockStatic(LocalDateTime.class, withSettings().useConstructor().defaultAnswer(CALLS_REAL_METHODS)));
            Mockito.when(LocalDateTime.now()).thenReturn(localDateTimes[0],
                    Arrays.stream(localDateTimes).skip(1).toArray(LocalDateTime[]::new));
            store.put(LOCAL_DATE_MOCK,
                    Mockito.mockStatic(LocalDate.class, withSettings().useConstructor().defaultAnswer(CALLS_REAL_METHODS)));
            Mockito.when(LocalDate.now()).thenReturn(localDateTimes[0].toLocalDate(),
                    Arrays.stream(localDateTimes).skip(1).map(LocalDateTime::toLocalDate).toArray(LocalDate[]::new));
            store.put(LOCAL_TIME_MOCK,
                    Mockito.mockStatic(LocalTime.class, withSettings().useConstructor().defaultAnswer(CALLS_REAL_METHODS)));
            Mockito.when(LocalTime.now()).thenReturn(localDateTimes[0].toLocalTime(),
                    Arrays.stream(localDateTimes).skip(1).map(LocalDateTime::toLocalTime).toArray(LocalTime[]::new));
        }

    }

    private void thenAnswer(Date[] mockDates, OngoingStubbing<Object> when) {
        when.then(invocation -> {
            Calendar instance = (Calendar) invocation.callRealMethod();
            instance.setTime(mockDates[0]);
            return instance;
        });
    }

    private LocalDateTime parseLocalDateTime(String dateString) {
        if (DEFAULT_FORMAT.length() != dateString.length()) {
            return null;
        }
        try {
            return LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern(DEFAULT_FORMAT));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Date parseDate(String dateString) {
        try {
            return DateUtils.parseDate(dateString,
                    "yyyy-MM-dd'T'HH:mm:ss'Z'",
                    "yyyy-MM-dd'T'HH:mm:ssZ",
                    "yyyy-MM-dd'T'HH:mm:ssz",
                    "yyyy-MM-dd'T'HH:mm:ssXXX",
                    "yyyy-MM-dd HH:mm:ss.SSS",
                    "yyyy-MM-dd HH:mm:ssXXX",
                    "yyyy-MM-dd HH:mm:ssZ",
                    "yyyy-MM-dd HH:mm:ss",
                    "yyyy-MM-dd HH:mm",
                    "yyyy-MM-ddXXX",
                    "yyyy-MM-dd",
                    "yyyy/MM/dd HH:mm:ssXXX",
                    "yyyy/MM/dd HH:mm:ssZ",
                    "yyyy/MM/dd HH:mm:ss",
                    "yyyy/MM/dd HH:mm",
                    "yyyy/MM/ddXXX",
                    "yyyy/MM/dd",
                    "yyyyMMddHHmmssXXX",
                    "yyyyMMddHHmmssZ",
                    "yyyyMMddHHmmss",
                    "yyyyMMddHHmm",
                    "yyyyMMddXXX",
                    "yyyyMMdd",
                    "EEE MMM ddHH:mm:ss 'GMT' yyyy",
                    "EEE MMM ddHH:mm:ss 'GST' yyyy");
        } catch (ParseException e) {
            throw new RuntimeException("mock的当前日期格式错误");
        }
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        final Store store = getStore(context);
        store.get(DATE_MOCKED_CONSTRUCTION, MockedConstruction.class).close();
        store.get(CALENDAR_MOCK, MockedStatic.class).close();
        MockedStatic localDateTimeMockStatic = store.get(LOCAL_DATE_TIME_MOCK, MockedStatic.class);
        if (localDateTimeMockStatic != null) {
            localDateTimeMockStatic.close();
        }
        MockedStatic localDateMockStatic = store.get(LOCAL_DATE_MOCK, MockedStatic.class);
        if (localDateMockStatic != null) {
            localDateMockStatic.close();
        }
        MockedStatic localTimeMockStatic = store.get(LOCAL_TIME_MOCK, MockedStatic.class);
        if (localTimeMockStatic != null) {
            localTimeMockStatic.close();
        }
    }

    private Store getStore(ExtensionContext context) {
        return context.getStore(Namespace.create(getClass(), context.getRequiredTestMethod()));
    }

}
