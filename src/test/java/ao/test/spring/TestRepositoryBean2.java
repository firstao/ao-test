package ao.test.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Repository;

/**
 * @author aohong
 */
@Repository
@Slf4j
@Offline
@Order(3)
public class TestRepositoryBean2 implements TestAble {

    public void test() {
        log.info("TestRepositoryBean test2");
    }
}
