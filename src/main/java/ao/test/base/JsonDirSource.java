/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * https://www.eclipse.org/legal/epl-v20.html
 */

package ao.test.base;

import org.apiguardian.api.API;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.lang.annotation.*;

import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * Json文件夹Source
 * 读取指定Json文件夹下面所有的Json文件并转化成测试方法的的参数，
 * 其中第一个参数为文件名（可选）用于显示，第二个参数为Json文件对应的java类型。
 *
 * @author aohong
 * @since junit 5.0
 * @see JsonDirSource
 * @see ArgumentsSource
 * @see org.junit.jupiter.params.ParameterizedTest
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@API(status = EXPERIMENTAL, since = "5.0")
@ArgumentsSource(JsonDirArgumentsProvider.class)
public @interface JsonDirSource {

	/**
	 * The Json dir classpath resources to use as the sources of arguments; must not be
	 * empty.
	 */
	String[] resources();

	/**
	 * java.util.Date类型数据的格式
	 * @return
	 */
	String dateFormat() default "yyyy-MM-dd HH:mm:ss";

}
