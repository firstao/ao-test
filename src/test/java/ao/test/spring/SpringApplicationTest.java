package ao.test.spring;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;

/**
 * @author aohong
 */
@SpringBootTest(classes = {TestServiceBean.class})
@Slf4j
public class SpringApplicationTest {

    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    TestServiceBean testServiceBean;
    @MockBean
    TestRepositoryBean testRepositoryBean;

    @Test
    void test() {
        testServiceBean.test();
        log.info(applicationContext.getBean("testServiceBean").toString());
        Mockito.verify(testRepositoryBean).test();
    }
}
