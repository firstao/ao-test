package ao.test.java8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ExampleTest {

    @Test
    public void testFunction1() {
        Function function = new Function() {
            @Override
            public Object apply(Object i) {
                return i;
            }
        };
        System.out.println(function);
        System.out.println(function.apply(1));
        Function function1 = i -> i;
        System.out.println(function1);
        System.out.println(function1.apply(1));
        Function function2 = function1.compose(i -> i);
        System.out.println(function2);
        System.out.println(function2.apply(1));
        Predicate predicate = i -> true;
        System.out.println(predicate);
        System.out.println(predicate.test(1));

        Consumer consumer = i -> System.out.println(i);
        System.out.println(consumer);
        consumer.accept(12);

        Supplier supplier = () -> 1;
        System.out.println(supplier);
        System.out.println(supplier.get());

        IntUnaryOperator intUnaryOperator = i -> i * i;
        System.out.println(intUnaryOperator);
        System.out.println(intUnaryOperator.applyAsInt(10));

        IntBinaryOperator add = (a, b) -> a + b;
        System.out.println(add);
        System.out.println(add.applyAsInt(1, 2));

        Integer integer = Stream.of(1, 2, 3, 4).reduce((a, b) -> a * b).get();
        System.out.println(integer);

        Integer integer1 = Stream.of(1, 2, 3, 4).reduce(5, (a, b) -> a * b);
        System.out.println(integer1);

        Double aDouble = Stream.of(1, 2, 3, 4, 4).reduce(15.1, (a, b) -> a + b, (u, a) -> u * a);
        System.out.println(aDouble);

        ArrayList<Integer> arrayList = Stream.of(1, 2, 3, 4).collect(ArrayList::new,
                ArrayList::add, (list, list1) -> {
                    list.remove(0);
                    list1.remove(0);
                });
        System.out.println(arrayList);

        Stream.of(1, 2, 3, 4).collect(Collectors.toList());

        System.out.println(Stream.of(1, 2, 3, 4).min(Comparator.naturalOrder()));
        System.out.println(Stream.of(1, 2, 3, 4).max(Integer::compareTo));
        System.out.println(IntStream.of(1, 2, 3, 4).max());

        System.out.println();
        System.out.println(arrayList.stream().reduce(100, (a, b) -> a + b, (a, b) -> {
            System.out.println(a);
            System.out.println(b);
            return a * b;
        }));
        System.out.println();
        System.out.println(arrayList.parallelStream().reduce(100, (a, b) -> a + b, (a, b) -> {
            System.out.println(a);
            System.out.println(b);
            return a * b;
        }));
    }

    @Test
    public void testFunction() {
        Function<Object, Object> function = i -> i;
        System.out.println(function);
//        System.out.println(i -> i);
        Object apply = function.apply("123");
        System.out.println(apply);
        Assertions.assertEquals("123", apply);
    }

    @Test
    public void testFunction2() {
        Function<String, Integer> converter = s -> Integer.parseInt(s);
        Integer converted = converter.apply("123");
        Assertions.assertEquals(123, converted);
    }

    @Test
    public void testFunctionReference() {
        Function<String, Integer> converter = Integer::valueOf;
        Integer converted = converter.apply("123");
        Assertions.assertEquals(123, converted);
    }

    @Test
    public void testFunctionReference1() {
        System.out.println();
        Consumer<String> converter = System.out::println;
        converter.accept("aaaa");

        BiConsumer<String, String> biConsumer = System.out::printf;
        biConsumer.accept("(%s)", "aaa");
    }

    @Test
    public void testFunctionReference2() {
        System.out.println();
        NoParameterFunction converter = System.out::println;
        converter.doSomething();
    }

    @Test
    public void testOptional() {
        Optional optional = Optional.empty();
        Assertions.assertFalse(optional.isPresent());
    }

    @Test
    public void testGroupBy() {
        Map<Integer, Long> map = Stream.of(1, 2, 3, 4, 1)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(map);
    }
}
