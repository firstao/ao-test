package ao.test.base;

import lombok.Data;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.Mockito;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;

/**
 * @author aohong
 */
public class MockitoTest {

    @Test
    void testMock3() {
        JsonDirArgumentsProvider mock = Mockito.mock(JsonDirArgumentsProvider.class);
        Arguments arguments1 = Arguments.of(1);
        Arguments arguments2 = Arguments.of(2);
        Mockito.doReturn(arguments1).when(mock).createArguments(eq(1), any());
        Mockito.doReturn(arguments2).when(mock).createArguments(2, null);
        Assertions.assertEquals(arguments1, mock.createArguments(1, null));
        Assertions.assertEquals(arguments2, mock.createArguments(2, null));
    }

    @Test
    void testSpy() {
        Date date = new Date();
        Date spy = spy(date);
        Date spy1 = spy(date);
        System.out.println(spy.getTime());
        System.out.println(spy1.getTime());
        System.out.println(spy.hashCode());
        System.out.println(spy1.hashCode());
        System.out.println(spy.equals(spy1));
    }

    @Data
    static class TestData {
        int a;

        void save(TestData testData) {
            this.a = add(testData.a);
            testData.a = this.a;
        }

        int add(int a) {
            return a + 1;
        }
    }

    @Test
    void testVerify() {
        TestData testData = Mockito.spy(TestData.class);
        System.out.println(testData.a);
        testData.save(new TestData());
        System.out.println(testData.a);
        Mockito.verify(testData).add(0);
//        Mockito.verify(testData).save(Mockito.refEq(new TestData()));
//        Mockito.verify(testData).save(JsonMatcher.eq(new TestData()));
    }
}
