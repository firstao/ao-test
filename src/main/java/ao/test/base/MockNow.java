/*
 * Copyright 2015-2019 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * https://www.eclipse.org/legal/epl-v20.html
 */

package ao.test.base;

import ao.test.base.extension.MockNowExtension;
import org.apiguardian.api.API;
import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.*;

import static org.apiguardian.api.API.Status.EXPERIMENTAL;

/**
 * 1. mock掉 new Date()方法，返回value设定的日期，如果设定了多个日期，则依次使用，使用次数超过设定数量，使用最后一个值。
 * <p>
 * 2. mock了Calendar.getInstance()和Calendar.getInstance(TimeZone.getTimeZone("GMT-0:00"))方法，暂时只支持mock一个时间
 * 实例 {测试代码 ao.test.base.MockNowTest}
 * <p>
 * 风险：hashCode和equals方法mock的有问题，会使用成Object的hashCode和equals方法，请谨慎使用
 * (所有的spy的对象都会有这个问题 {测试代码 ao.test.base.MockitoTest#testSpy()})
 * @author aohong
 * @since 5.0
 * @see MockNowExtension
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@API(status = EXPERIMENTAL, since = "5.0")
@ExtendWith(MockNowExtension.class)
public @interface MockNow {
	String DEFAULT_MOCK_DATE = "2021-01-01 18:11:12+08:00";

	String[] value() default DEFAULT_MOCK_DATE;
}
