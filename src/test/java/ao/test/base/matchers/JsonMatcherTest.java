package ao.test.base.matchers;

import ao.test.base.AssertJsonEquals;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static ao.test.base.AssertJsonEquals.assertJsonEquals;

/**
 * JsonMatcher Tester.
 *
 * @author aohong
 */
public class JsonMatcherTest {

    @Test
    public void testEq() throws Exception {
        List<TestData> list = Mockito.mock(List.class);
        TestData testData = new TestData(1);
        TestData testData1 = new TestData(1);
        AssertJsonEquals.assertJsonEquals(testData, testData1);
        // 不相等
//        Assertions.assertEquals(testData, testData1);
        list.add(testData);
        // 不通过
//        Mockito.verify(list).add(testData1);
        Mockito.verify(list).add(JsonMatcher.eq(testData1));
        // 不通过
//        Mockito.verify(list).add(JsonMatcher.eq(new TestData(2)));
    }

    @Test
    void testMock() {
        List<TestData> list = Mockito.mock(List.class);
        Mockito.when(list.add(JsonMatcher.eq(new TestData(1)))).thenReturn(true);
        final boolean result = list.add(new TestData(1));
        Assertions.assertTrue(result);
        Mockito.verify(list).add(JsonMatcher.eq(new TestData(1)));
        // 不通过
//        Mockito.verify(list).add(AssertJsonMatcher.eq(new TestData(2)));
    }

}
