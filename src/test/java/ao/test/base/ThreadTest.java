package ao.test.base;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {

    @Test
    void test() throws InterruptedException {
        Thread thread = new Thread(() -> {
            Thread currentThread = Thread.currentThread();
            System.out.println(currentThread.isInterrupted());
            while (!currentThread.isInterrupted()) {
                System.out.println(System.currentTimeMillis());
                System.out.println(currentThread.isInterrupted());
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
////                    throw new RuntimeException(e);
//                }
                synchronized (currentThread) {
                    currentThread.notify();
                }
                System.out.println(currentThread.isInterrupted());
            }
        });
        thread.start();
        System.out.println("start:" + thread.isInterrupted());
        Thread.sleep(10);
        synchronized (thread) {
            thread.wait();
        }
        thread.interrupt();
        System.out.println("interrupt:" + thread.isInterrupted());
        thread.join();
    }

    @Test
    void pool() {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.execute(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println(i);
            }
        });
        pool.shutdownNow();
    }

    @Test
    void pool1() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.execute(() -> {
            int i = 0;
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println(i++);
            }
        });
        Thread.sleep(1);
        pool.shutdownNow();
    }
}
