package ao.test.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author aohong
 */
@SpringBootApplication
@Slf4j
public class SpringApplication {

}
