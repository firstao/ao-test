# ao-test
## 描述
基于Junit5、JSON、Mockito3的单元测试基础项目

## maven引用方式
```
<dependency>
  <groupId>io.gitee.firstao</groupId>
  <artifactId>ao-test</artifactId>
  <version>1.0</version>
  <scope>test</scope>
</dependency>
```

## 主要功能描述
### @JsonDirSource
结合@ParameterizedTest使用，文件夹内每个测试用例使用单个json文件保存，解析出的字段包含测试方法的参数、mock的数据、assert的数据

### AssertJsonEquals
将对象转换成json后对比，即对比两个对象是否值相等，用于断言没有实现equals方法的pojo对象是否值相等。

### JsonMatcher.eq(Object wanted)
参数匹配器，以json序列化后的值进行匹配，用于mock

### @MockNow
mock掉 new Date()方法，返回value设定的日期，如果设定了多个日期，则依次使用，使用次数超过设定数量，使用最后一个值。

实例 {@link ao.test.base.MockNowTest}

### @JsonPrinter
Mockito.verify校验失败时，使用json格式对比参数，并打印出来

示例
![输入图片说明](doc/%E5%AE%9E%E4%BE%8B%E5%9B%BE%E7%89%87.png)
![输入图片说明](doc/json%E6%96%87%E4%BB%B6.png)