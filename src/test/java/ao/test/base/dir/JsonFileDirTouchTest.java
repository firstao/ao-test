package ao.test.base.dir;

import ao.test.base.JsonDirSource;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;

/**
 * JsonFileDirTestBase
 *
 * @author aohong
 */
class JsonFileDirTouchTest {

    @Data
    static class TestData {
        int param;
        int result;
    }

    @BeforeEach
    public void before() throws Exception {
        System.out.println("--------before------");
    }

    @AfterEach
    public void after() throws Exception {
        System.out.println("--------after------");
    }

    @ParameterizedTest
    @JsonDirSource(resources = "/data/JsonFileDirTouchTest/touch")
    public void testTouch(String fileName, TestData testData) {
        System.out.println(fileName);
        Assertions.assertEquals(testData.result, testData.param);
    }

}
