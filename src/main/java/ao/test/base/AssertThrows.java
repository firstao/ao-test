package ao.test.base;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

/**
 * 断言异常工具类
 *
 * @author aohong
 */
public class AssertThrows {

    private AssertThrows() {
    }

    /**
     * 断言executable是否抛指定异常
     * expectedType为null则断言不抛异常，不为null断言抛指定异常且message一致
     *
     * @param expectedType
     * @param message
     * @param executable
     * @param <T>
     */
    public static <T extends Throwable> T assertIsThrow(Class<T> expectedType, String message, Executable executable) {
        if (expectedType == null) {
            Assertions.assertDoesNotThrow(executable);
            return null;
        } else {
            final T throwable = Assertions.assertThrows(expectedType, executable);
            Assertions.assertEquals(message, throwable.getMessage());
            return throwable;
        }
    }

}
