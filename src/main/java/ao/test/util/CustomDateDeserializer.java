package ao.test.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CustomDateDeserializer extends JsonDeserializer<Date> {
    private final List<String> dateFormats;

    public CustomDateDeserializer(List<String> dateFormats) {
        this.dateFormats = dateFormats;
    }

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String date = jsonParser.getText();
        for (String format : dateFormats) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(format);
                return sdf.parse(date);
            } catch (ParseException e) {
                // Ignore and try the next format
            }
        }
        throw new JsonParseException(jsonParser, "Unparseable date: \"" + date + "\". Supported formats: " + String.join(", ", dateFormats));
    }
}
