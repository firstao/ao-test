package ao.test.base;

import ao.test.util.JsonFileReadUtil;
import ao.test.util.JsonUtil;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.support.AnnotationConsumer;
import org.junit.platform.commons.util.Preconditions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;

import static ao.test.util.JsonUtil.DATE_FORMAT;

/**
 * Json文件参数提供者
 * @author aohong
 * @since junit 5.0
 */
class JsonDirArgumentsProvider implements ArgumentsProvider, AnnotationConsumer<JsonDirSource> {

    private JsonDirSource annotation;

    @Override
    public void accept(JsonDirSource jsonDirSource) {
        this.annotation = jsonDirSource;
    }

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        Method testMethod = context.getRequiredTestMethod();
        Class<?>[] parameterTypes = testMethod.getParameterTypes();
        int parameterSize = parameterTypes.length;
        Preconditions.condition(parameterSize >= 1 && parameterSize <= 2,
                () -> String.format("@JsonDirSource 注解不能使用在无参或超过两个参数的方法上[%s]，" +
                                "其中第一个参数为文件名（可选）用于显示，第二个参数为Json文件对应的class类型。",
                        testMethod.toGenericString()));
        final Class<?> clazz = parameterTypes[parameterSize - 1];

        DATE_FORMAT.set(annotation.dateFormat());
        return Arrays.stream(annotation.resources())
                .peek(resource -> init(resource, clazz))
                .map(resource -> JsonFileReadUtil.readJsonObjectDir(resource, clazz))
                .map(SortedMap::entrySet)
                .flatMap(Collection::stream)
                .map(entry -> createArguments(parameterSize, entry));
    }

    /**
     *
     * @param resource
     * @param clazz
     */
    private void init(String resource, Class<?> clazz) {
        final String pathname = "src/test/resources" + resource;
        final File dir = new File(pathname);
        if (dir.exists()) {
            return;
        }
        final File file = new File(pathname + "/001.json");
        try {
            FileUtils.touch(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (FileOutputStream fos = new FileOutputStream(file)) {
            Constructor<?> declaredConstructor = clazz.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            fos.write(JsonUtil.toJson(declaredConstructor.newInstance(), true, false).getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Arguments createArguments(int parameterSize, Map.Entry<String, ?> entry) {
        if (parameterSize == 1) {
            return Arguments.of(entry.getValue());
        } else {
            return Arguments.of(entry.getKey(), entry.getValue());
        }
    }

}
