package ao.test.spring;

/**
 * @author aohong
 */
public interface TestAble {
    void test();
}
