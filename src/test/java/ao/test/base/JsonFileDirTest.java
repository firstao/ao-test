package ao.test.base;

import ao.test.util.JsonFileReadUtil;
import ao.test.util.JsonUtil;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

/**
 * JsonFileDirTestBase
 *
 * @author aohong
 */
class JsonFileDirTest {

    public String fileName;

    @Data
    static class TestData {
        int param;
        int result;
    }

    @BeforeEach
    public void before() throws Exception {
        System.out.println("--------before------");
    }

    @AfterEach
    public void after() throws Exception {
        System.out.println("--------after------");
    }

    public static Set<Map.Entry<String, TestData>> list() throws Exception {
        return JsonFileReadUtil
                .readJsonObjectDir("/data1/", TestData.class)
                .entrySet();
    }

    @ParameterizedTest
    @MethodSource("list")
    public void test(Map.Entry<String, TestData> entry) {
        this.fileName = entry.getKey();
        TestData testData = entry.getValue();
        Assertions.assertEquals(testData.result, testData.param * 10);
    }

    @ParameterizedTest
    @MethodSource
    public void list(Map.Entry<String, TestData> entry) {
        this.fileName = entry.getKey();
        TestData testData = entry.getValue();
        Assertions.assertEquals(testData.result, testData.param * 10);
    }

    @ParameterizedTest
    @JsonDirSource(resources = "/data1")
    public void test2(TestData testData) {
        Assertions.assertEquals(testData.result, testData.param * 10);
    }

    @ParameterizedTest
    @JsonDirSource(resources = "/data1/")
    public void test2(String fileName, TestData testData) {
        System.out.println(fileName);
        Assertions.assertEquals(testData.result, testData.param * 10);
    }

    @Data
    static class TestDateData {
        Date date;
        Date date1;
        Calendar calendar;
        LocalDateTime localDateTime;
        ZonedDateTime zonedDateTime;
        LocalDate localDate;
        LocalTime localTime;
        String result;
    }

    @ParameterizedTest
    @JsonDirSource(resources = "/data/date")
    public void date(String fileName, TestDateData testData) {
        String result = testData.result;
        testData.setResult(null);
        System.out.println(JsonUtil.toJson(testData));
        Assertions.assertEquals(result, JsonUtil.toJson(testData));
    }

    @ParameterizedTest
    @JsonDirSource(resources = "/data/date1", dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    public void date1(String fileName, TestDateData testData) {
        String result = testData.result;
        testData.setResult(null);
        System.out.println(JsonUtil.toJson(testData));
        Assertions.assertEquals(result, JsonUtil.toJson(testData));
    }

    @Test
    void test() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SSSXXX", Locale.CANADA);
        System.out.println(sdf1.format(new Date()));
        sdf1.setTimeZone(TimeZone.getTimeZone(ZoneOffset.UTC));
        System.out.println(sdf1.format(new Date()));
        System.out.println(ZoneOffset.UTC);
    }

}
