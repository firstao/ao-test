package ao.test.java8;

import java.lang.annotation.Repeatable;

@Repeatable(Hints.class)
@interface Hint {
    String value();
}
