package ao.test.util;

import org.apache.commons.io.FileUtils;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;


public final class JsonFileReadUtil {

    private JsonFileReadUtil() {

    }

    /**
     * 读取文件文本内容
     *
     * @param pathname 文件路径
     * @return 文件文本内容
     */
    public static String readFile(String pathname) {
        URL url = JsonFileReadUtil.class.getResource(pathname);
        File file = FileUtils.toFile(url);
        return readFile(file);
    }

    public static String readFile(File file) {
        try {
            return FileUtils.readFileToString(file, "UTF8");
        } catch (IOException e) {
            throw new RuntimeException("读取文件失败", e);
        }
    }

    public static <T> T readObjectFromJsonFile(String pathname, Class<T> tClass) {
        return JsonUtil.fromJson(JsonFileReadUtil.readFile(pathname), tClass);
    }

    /**
     * 读取一个文件夹下面的所有文本文件
     *
     * @param dir 文件夹
     * @return 文件名和文件内容的map
     */
    public static <T> SortedMap<String, T> readDir(File dir, Function<File, T> valueFunction) {
        Assert.isTrue(dir.isDirectory(), "dirName不是目录");
        File[] files = dir.listFiles();
        Assert.notNull(files, "没有文件");
        SortedMap<String, T> map = new TreeMap<>();
        for (File file : files) {
            map.put(file.getName(), valueFunction.apply(file));
        }
        return map;
    }
    public static <T> SortedMap<String, T> readDir(URL url, Function<File, T> valueFunction) {
        return readDir(FileUtils.toFile(url), valueFunction);
    }

    public static SortedMap<String, String> readStringFromDir(String dirName) {
        URL url = JsonFileReadUtil.class.getResource(dirName);
        File dir = FileUtils.toFile(url);
        return readDir(dir, JsonFileReadUtil::readFile);
    }

    /**
     * 读取一个Json文件夹下面的所有Json格式文件并转换成对象返回
     *
     * @param dir 文件夹
     * @param tClass  对象Class
     * @param <T>     对象类型
     * @return 文件名和对象的map
     */
    public static <T> SortedMap<String, T> readObjectFromJsonFileDir(File dir, Class<T> tClass) {
        return readDir(dir, file -> JsonUtil.fromJson(JsonFileReadUtil.readFile(file), tClass));
    }

    /**
     * @see #readJsonObjectDir
     * @param tClass
     * @param <T>
     * @return
     */
    public static <T> SortedMap<String, T> readObjectFromJsonFileDir(URL url, Class<T> tClass) {
        return readObjectFromJsonFileDir(FileUtils.toFile(url), tClass);
    }
    public static <T> SortedMap<String, T> readJsonObjectDir(String dirName, Class<T> tClass) {
        URL url = JsonFileReadUtil.class.getResource(dirName);
        return readObjectFromJsonFileDir(FileUtils.toFile(url), tClass);
    }
}
