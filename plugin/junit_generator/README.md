### 单元测试代码生成器配置
output path（生成代码路径）:`${SOURCEPATH}/../../test/java/${PACKAGE}/${FILENAME}`

模板分类：
- junit5-default.vm：junit5默认的单元测试，用于测试util等不需要依赖任何外部系统的代码
- junit5-mock.vm：使用mock方式的单元测试，只测试单个方法，所有的DB操作、调用的外部接口全部为mock的
- junit5-SpringBootTest.vm：
    1. 测试repository和mapper，使用h2准备数据
    2. 使用SpringBootTest的集成单元测试，测试对外的接口，使用的数据为调用其他接口准备的，调用其他系统的接口为mock的

