package ao.test.base.matchers;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * JsonEquals Tester.
 *
 * @author aohong
 */
public class JsonEqualsTest {

    /**
     * Method: matches(Object argument)
     */
    @Test
    public void testMatches() throws Exception {
        TestData mock = Mockito.mock(TestData.class);
        final TestData testData = new TestData(1);
        final TestData testData1 = new TestData(1);
        System.out.println(testData.equals(testData1));

        mock.test(testData);
        Mockito.verify(mock).test(testData);
        // 报错,对比的对象地址
//        Mockito.verify(mock).test(testData1);
        Mockito.verify(mock).test(Mockito.refEq(testData1));
        Mockito.verify(mock, Mockito.never()).test(testData1);
        Mockito.verify(mock, Mockito.never()).test(Mockito.eq(testData1));
//        Mockito.verify(mock).setData(Mockito.argThat(new JsonEquals(ResResult.success())));
//        Mockito.verify(mock).test(Mockito.argThat(new JsonEquals(testData1)));

    }


} 
