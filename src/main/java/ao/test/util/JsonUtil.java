//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ao.test.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.json.PackageVersion;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class JsonUtil {
    public static ThreadLocal<String> DATE_FORMAT = new ThreadLocal<>();

    private static final ObjectMapper useAnnotationMapper = new ObjectMapper();
    private static final ObjectMapper excludeNullMapper = new ObjectMapper();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZZZ");
    static {
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        initObjectMapper(useAnnotationMapper);
        initObjectMapper(excludeNullMapper);
        excludeNullMapper.setSerializationInclusion(Include.NON_NULL);
    }

    private JsonUtil() {
    }

    public static ObjectMapper getUseAnnotationMapper() {
        return useAnnotationMapper;
    }

    private static void initObjectMapper(ObjectMapper objectMapper) {
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
        objectMapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
        objectMapper.configure(Feature.IGNORE_UNDEFINED, true);
        objectMapper.configure(MapperFeature.USE_ANNOTATIONS, true);
        objectMapper.configure(com.fasterxml.jackson.core.JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        objectMapper.setDateFormat(dateFormat);
        SimpleModule module = new SimpleModule("dateTime", PackageVersion.VERSION);
        module.addDeserializer(Date.class, new CustomDateDeserializer(Arrays.asList(
                "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
                "yyyy-MM-dd HH:mm:ssZZZ",
                "yyyy-MM-dd'T'HH:mm:ssZZZ",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd'T'HH:mm:ss",
                "yyyy-MM-dd"
        )));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dateTimeFormatter));
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        module.addSerializer(LocalDate.class, new LocalDateSerializer(dateFormatter));
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer(dateFormatter));
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        module.addSerializer(LocalTime.class, new LocalTimeSerializer(timeFormatter));
        module.addDeserializer(LocalTime.class, new LocalTimeDeserializer(timeFormatter));
        objectMapper.registerModule(module);
        objectMapper.registerModule(new JavaTimeModule());
    }

    public static String toJson(Object obj, boolean prettyPrinter, boolean excludeNull) {
        if (null == obj) {
            return "";
        }

        if (obj instanceof CharSequence) {
            return obj.toString();
        }

        ObjectMapper objectMapper;
        if (excludeNull) {
            objectMapper = excludeNullMapper;
        } else {
            objectMapper = useAnnotationMapper;
        }

        setDateFormat(objectMapper);
        try {
            if (prettyPrinter) {
                return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
            } else {
                return objectMapper.writeValueAsString(obj);
            }
        } catch (Exception e) {
            throw new RuntimeException(String.format("将%s得对象转换为Json字符串出错,%s", obj.getClass(), e.getCause()), e);
        }
    }

    private static void setDateFormat(ObjectMapper objectMapper) {
        String localDateFormat = DATE_FORMAT.get();
        if (StringUtils.hasText(localDateFormat)) {
            objectMapper.setDateFormat(new SimpleDateFormat(localDateFormat, Locale.CHINA));
        } else {
            objectMapper.setDateFormat(dateFormat);
        }
    }

    public static String toJson(Object obj, boolean prettyPrinter) {
        return toJson(obj, prettyPrinter, true);
    }

    public static String toJson(Object obj) {
        return toJson(obj, false);
    }

    public static <T> T fromJson(String content, Class<T> resultClazz, Class<?>... parameters) {
        return fromJson(content, JavaTypeFactory.constructType(resultClazz, parameters));
    }

    public static <T> T fromJson(String content, JavaType javaType) {
        if (!StringUtils.hasText(content)) {
            return null;
        }

        Assert.notNull(javaType, "javaType 不能为null");

        try {
            setDateFormat(useAnnotationMapper);
            return useAnnotationMapper.readValue(content, javaType);
        } catch (Exception e) {
            throw new RuntimeException(String.format("将%s解析为%s出错,%s", content, javaType.getRawClass(), e.getCause()), e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T clone(T orgObj) {
        return (T) copy(orgObj, orgObj.getClass());
    }

    public static <T> T copy(Object orgObj, Class<T> descClazz, Class<?>... parameters) {
        return orgObj == null ? null : fromJson(toJson(orgObj), descClazz, parameters);
    }

}
